#!/bin/bash

RESULTS=$PWD/results/ldpc_enc
RESULTS_JERASURE=$PWD/results/jerasure
RESULTS_JERASURE_RAID=$PWD/results/jerasure-raid
RESULTS_LDPC=$PWD/results/ldpc-results
RESULTS_LIBFEC=$PWD/results/libfec
RESULTS_OPENFEC=$PWD/results/openfec-ldpc

# delete all eps files from results
[ -n "$1" ] && [ $1 = "r" ] && rm $RESULTS_JERASURE/*.eps $RESULTS_JERASURE_RAID/*.eps \
$RESULTS_LIBFEC/*.eps $RESULTS_LDPC/*.eps $RESULTS/*.eps && exit 0

# plot all graphs in results
cd $RESULTS && gnuplot *.dem || echo "[NOK]"
cd $RESULTS_JERASURE && gnuplot *.dem || echo "[NOK]"
cd $RESULTS_JERASURE_RAID && gnuplot *.dem || echo "[NOK]"
cd $RESULTS_LDPC && gnuplot *.dem || echo "[NOK]"
cd $RESULTS_LIBFEC && gnuplot *.dem || echo "[NOK]"