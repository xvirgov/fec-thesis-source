/*#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>*/
#include <cstdint>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "cauchy_256.h"
#include "tests/SiameseTools.h"

using namespace std;

/*void printBuff(char *buff, size_t size) {
	printf("\n");
	for(int i = 0; i < size; i++) {
		printf("%d ", buff[i]);
		if(i!=0 && i%32==0) {
			printf("\n");
		}
	}
}*/
const int ITERATIONS=1000;

/*long time_ms(struct rusage *start, struct rusage *end)
{
	long ms;

	ms = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000;
	ms += (end->ru_utime.tv_usec - start->ru_utime.tv_usec)/1000;

	return ms;
}

long timespec_ms(struct timespec *start, struct timespec *end) {
	return (end->tv_sec - start->tv_sec) * 1000 +
			(end->tv_nsec - start->tv_nsec) / (1000 * 1000);
}*/

int main(int argc, const char * argv[]) {
	if(argc < 3) return -1;
	int k = atoi(argv[1]); // number of blocks of original data (k), which must be less than 256
	int m = atoi(argv[2]);  // number of blocks of redundant data (m), which must be no more than 256 - k
	int bytes = 4096; // number of bytes per block (bytes), which must be a multiple of 8 bytes
	// Device is divided into <dev_size>/(k*bytes) pieces, for each piece are generated m parity blocks
	int nPieces = 1000;
	int buff_size = nPieces * k * bytes;
	int parity_size = nPieces * m * bytes;

	//struct rusage rstart, rend;
	//struct timespec tstart, tend;
	//long ms = 0;
	siamese::PCGRandom prng;
	prng.Seed(siamese::GetTimeUsec());
	uint64_t sum_encode = 0;

	int i, j;

	unsigned char *buff = (unsigned char *)malloc(buff_size);
	for(int i = 0; i < buff_size; i++) {
		buff[i] = i%26 + 97;
	}
	unsigned char *cpy = (unsigned char *)malloc(buff_size);
	memcpy(cpy, buff, buff_size);
	unsigned char *parity = (unsigned char *)malloc(parity_size);

	memset(buff, 0, buff_size);

	const unsigned char *buffPtr[k];

	if (cauchy_256_init()) {
		free(buff);
		free(cpy);
		free(parity);
		return -1;
	}
	memcpy(buff, cpy, buff_size);

for(int iter = 0; iter < ITERATIONS; iter++) {
	memset(parity, 0, parity_size);
	for(i = 0; i < nPieces; i++) {
		for(j = 0; j < k; j++) {
			buffPtr[j] = &buff[i*k*bytes + j*bytes];
		}

		/*if(clock_gettime(CLOCK_MONOTONIC_RAW, &tstart) < 0) {
			return -1;
		}*/
		/*if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			free(buff);
			free(cpy);
			free(parity);
			return -1;
		}*/


//printBuff(&parity[i*m*bytes], bytes);
		const uint64_t t0 = siamese::GetTimeUsec();
		if(cauchy_256_encode(k, m, buffPtr, &parity[i*m*bytes], bytes)) {
			free(buff);
			free(cpy);
			free(parity);
			return -1;
		}
		const uint64_t t1 = siamese::GetTimeUsec();
		const uint64_t encode_time = t1 - t0;
		sum_encode += encode_time;

		//printf("==============================================================\n");
		//printBuff(&parity[i*m*bytes], bytes);
		//return 1;

		/*if(clock_gettime(CLOCK_MONOTONIC_RAW, &tend) < 0) {
			return -1;
		}*/


		/*if(getrusage(RUSAGE_SELF, &rend) < 0) {
			free(buff);
			free(cpy);
			free(parity);
			return -1;
		}*/

		//ms += timespec_ms(&tstart, &tend);
		//printf("%ld\n", ms);
		//ms += time_ms(&rstart, &rend);
		//printf("Block %d\n", i);
		// Decoding of blocks
		// Block info - contains blocks and index of blocks (indexing from zero, 0 to (k-1) - data blocks, k to (k+m-1) - recovery blocks )
		// So, if out of k+m blocks, m fails, we can recover all data blocks -> MDS code
		/*Block block_info[k];
		//printBuff(&buff[i*k*bytes], bytes);
		// last m blocks failed -> first k-m blocks are uncorrupted data blocks, next m are parity blocks
		for(j = 0; j < k-m; j++) {
			block_info[j].data = &buff[i*k*bytes + j*bytes];
			block_info[j].row = j;
		}

		for(j = k-m; j < k; j++) {
			memset(&buff[i*k*bytes + j*bytes], 0, bytes);
		}

		l = 0;
		for(j = k-m; j < k; j++) {
			block_info[j].data = &parity[i*m*bytes + l*bytes];
			block_info[j].row = k+l;
			l++;
		}

		// block-info elements that used to have redundant data are corrected in-place
		if(cauchy_256_decode(k, m, block_info, bytes)) {
			free(buff);
			free(cpy);
			free(parity);
			return -1;
		}

		l = 0;
		for(j = k-m; j < k; j++) {
			memcpy(&buff[i*k*bytes + j*bytes], &parity[i*m*bytes + l*bytes], bytes);
			l++;
		}

		if(memcmp(cpy, buff, buff_size) != 0) {
			free(buff);
			free(cpy);
			free(parity);
			printf("[NOK]\n");
			return -1;
		}
		//printf("Block %d\n", i);*/

	}
	//printf("%d\n", iter);
}

//cout << ITERATIONS << " " << buff_size << " Encoded " << buff_size/1024/1024*ITERATIONS << " MB  in " << sum_encode << " usec." << endl;
cout << round((buff_size)/1024/1024*ITERATIONS / (sum_encode/1000000.));
	/*printf("%d x %d\n", buff_size, ITERATIONS);
	printf("%ld\n", ms);
	printf("SPEED:%f (MiB/sec)\n", ((buff_size/1024/1024)*ITERATIONS)/(ms/1000.) );
	printf("longhair [OK]\n");*/


	free(buff);
	free(cpy);
	free(parity);

	return 0;
}
