#!/bin/bash

# tests for library ISA-L
# this test requires compiled file libs/isa-l_encode_decode.c

WD=$PWD
IWD=$PWD/isa-l-master
EIWD=$IWD/examples/ec

# first time
# ./autogen.sh && ./configure && make
cd $IWD
make ex

cd $EIWD
./ec_simple_example -k 6 -p 2 -l 4096 -e 1 -e 7
./ec_simple_example -k 14 -p 7 -l 4096 -e 3 -e 6