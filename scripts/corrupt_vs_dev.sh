#!/bin/bash

N=$1
K=$2
BS=4096
DEV_SIZE=262144000

# interleaving takes K blocks and then takes one byte from each block to encode (this is repeated <block_size> times)
# rounds = ((device size) / (block size)) / (number of source symbols)
ROUNDS=$(($DEV_SIZE / $BS))
ROUNDS=$(($ROUNDS / $K))

DEV=tst_dev

# this code is mds -> can repair count=(n-k)/2 errors
COUNT=$(($N-$K))
COUNT=$(($COUNT/2))

cd ../libs/Libfec_Veritysetup/cryptsetup-master/

for i in `seq 10 $ROUNDS`
do
	POS=$(($K*$i))
	# corruption - rewrite random data by zeroes
	dd if=/dev/zero of=$DEV seek=$POS bs=$BS count=$COUNT conv=notrunc > /dev/null 2>&1
done