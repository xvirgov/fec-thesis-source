#!/bin/bash

CS=LibFEC_Veritysetup/cryptsetup-master

cd $CS
pwd
for i in `seq 2 32`
do
	./veritysetup format tst_dev tst_hash --fec-device tst_fec --fec-roots $i --debug | grep -oP "(?<=Encoding_speed: )[^ ]+"
done