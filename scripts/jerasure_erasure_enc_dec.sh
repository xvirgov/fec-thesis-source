#!/bin/bash

SWD=$PWD/../src
JD=$PWD/../libs/jerasure.git
ED=$JD/Examples

CSV_FILE_62_ENC=$PWD/../results/jerasure/jerasure_erasures_6_2_enc.csv
CSV_FILE_62_DEC=$PWD/../results/jerasure/jerasure_erasures_6_2_dec.csv
CSV_FILE_147_ENC=$PWD/../results/jerasure/jerasure_erasures_14_7_enc.csv
CSV_FILE_147_DEC=$PWD/../results/jerasure/jerasure_erasures_14_7_dec.csv

cd $JD && make
rm $CSV_FILE_147_ENC $CSV_FILE_62_DEC $CSV_FILE_147_DEC $CSV_FILE_62_ENC

DEV=tst

function remove8() {
	NAMES=('tst_k1' 'tst_k2' 'tst_k3' 'tst_k4' 'tst_k5' 'tst_k6' 'tst_m1' 'tst_m2')
	cd $SWD
	RAND_I=(`./random_seq 6 2`)
	cd $ED
	rm Coding/${NAMES[${RAND_I[0]}]} Coding/${NAMES[${RAND_I[1]}]}
}

function remove21() {
	NAMES=('tst_k01' 'tst_k02' 'tst_k03' 'tst_k04' 'tst_k05' 'tst_k06' 'tst_k07' 'tst_k08' 'tst_k09' 'tst_k10' 'tst_k11' 'tst_k12' 'tst_k13' 'tst_k14' 'tst_m01' 'tst_m02' 'tst_m03' 'tst_m04' 'tst_m05' 'tst_m06' 'tst_m07')
	cd $SWD
	RAND_I=(`./random_seq 14 7`)
	cd $ED
	for rr in `seq 1 7`;
	do
		rm Coding/${NAMES[${RAND_I[$rr]}]}
	done
}

cd $ED
dd if=/dev/urandom of=$DEV bs=1M count=500

K=6
M=2
PS=4096
ITER=10
for i in `seq 1 32`
do
	cd $ED
	############
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 8 ] &&
	for ii in `seq 1 $ITER`;
	do
		RS_ENC=$(./encoder $DEV $K $M reed_sol_van $i 0 0) && remove8 && RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$RS_ENC))
		SUM_DEC=$(($SUM_DEC+$RS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))

	#############
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 16 ] &&
	for ii in `seq 1 $ITER`;
	do
		RS_ENC=$(./encoder $DEV $K $M reed_sol_van $i 0 0) && remove8 && RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$RS_ENC))
		SUM_DEC=$(($SUM_DEC+$RS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))

	#############
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 32 ] &&
	for ii in `seq 1 $ITER`;
	do
		RS_ENC=$(./encoder $DEV $K $M reed_sol_van $i 0 0) && remove8 && RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$RS_ENC))
		SUM_DEC=$(($SUM_DEC+$RS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))

	############## Cauchy RS
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 8 ] &&
	for ii in `seq 1 $ITER`;
	do
		CRS_ENC=$(./encoder $DEV $K $M cauchy_good $i $PS 0) && remove8 && CRS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		[ "$CRS_ENC" != "" ] && SUM_ENC=$(($SUM_ENC+$CRS_ENC))
		[ "$CRS_DEC" != "" ] && SUM_DEC=$(($SUM_DEC+$CRS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && CRS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && CRS_DEC=$(($SUM_DEC/$ITER))

	################
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 16 ] &&
	for ii in `seq 1 $ITER`;
	do
		CRS_ENC=$(./encoder $DEV $K $M cauchy_good $i $PS 0) && remove8 && CRS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		[ "$CRS_ENC" != "" ] && SUM_ENC=$(($SUM_ENC+$CRS_ENC))
		[ "$CRS_DEC" != "" ] && SUM_DEC=$(($SUM_DEC+$CRS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && CRS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && CRS_DEC=$(($SUM_DEC/$ITER))

	################
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 32 ] &&
	for ii in `seq 1 $ITER`;
	do
		CRS_ENC=$(./encoder $DEV $K $M cauchy_good $i $PS 0) && remove8 && CRS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		[ "$CRS_ENC" != "" ] && SUM_ENC=$(($SUM_ENC+$CRS_ENC))
		[ "$CRS_DEC" != "" ] && SUM_DEC=$(($SUM_DEC+$CRS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && CRS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && CRS_DEC=$(($SUM_DEC/$ITER))

	#sleep 5
	LINE_ENC="$i,$RS_ENC,$CRS_ENC"
	LINE_DEC="$i,$RS_DEC,$CRS_DEC"
	#[ -z $RS ] && WORD="$i $CRS"
	echo $LINE_ENC >> $CSV_FILE_62_ENC
	echo $LINE_DEC >> $CSV_FILE_62_DEC
	echo "$LINE_ENC ::: $LINE_DEC"
	unset LINE_ENC LINE_DEC RS_ENC RS_DEC CRS_ENC CRS_DEC
done

#K=14
#M=7
#for i in `seq 1 32`
#do
#	cd $ED
#	############
#	SUM_ENC=0
#	SUM_DEC=0
#	[ $i -eq 8 ] &&
#	for ii in `seq 1 $ITER`;
#	do
#		RS_ENC=$(./encoder $DEV $K $M reed_sol_van $i 0 0) && remove21
#		RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
#		SUM_ENC=$(($SUM_ENC+$RS_ENC))
#		SUM_DEC=$(($SUM_DEC+$RS_DEC))
#	done
#	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
#	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))
#
#	#############
#	SUM_ENC=0
#	SUM_DEC=0
#	[ $i -eq 16 ] &&
#	for ii in `seq 1 $ITER`;
#	do
#		RS_ENC=$(./encoder $DEV $K $M reed_sol_van $i 0 0) && remove21
#		RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
#		SUM_ENC=$(($SUM_ENC+$RS_ENC))
#		SUM_DEC=$(($SUM_DEC+$RS_DEC))
#	done
#	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
#	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))
#
#	#############
#	SUM_ENC=0
#	SUM_DEC=0
#	[ $i -eq 32 ] &&
#	for ii in `seq 1 $ITER`;
#	do
#		RS_ENC=$(./encoder $DEV $K $M reed_sol_van $i 0 0) && remove21
#		RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
#		SUM_ENC=$(($SUM_ENC+$RS_ENC))
#		SUM_DEC=$(($SUM_DEC+$RS_DEC))
#	done
#	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
#	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))
#
#	############## Cauchy RS
#	SUM_ENC=0
#	SUM_DEC=0
#	[ $i -eq 8 ] &&
#	for ii in `seq 1 $ITER`;
#	do
#		CRS_ENC=$(./encoder $DEV $K $M cauchy_good $i $PS 0) && remove21
#		CRS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
#		[ "$CRS_ENC" != "" ] && SUM_ENC=$(($SUM_ENC+$CRS_ENC))
#		[ "$CRS_DEC" != "" ] && SUM_DEC=$(($SUM_DEC+$CRS_DEC))
#	done
#	[ $SUM_ENC -ne 0 ] && CRS_ENC=$(($SUM_ENC/$ITER))
#	[ $SUM_DEC -ne 0 ] && CRS_DEC=$(($SUM_DEC/$ITER))
#
#	################
#	SUM_ENC=0
#	SUM_DEC=0
#	[ $i -eq 16 ] &&
#	for ii in `seq 1 $ITER`;
#	do
#		CRS_ENC=$(./encoder $DEV $K $M cauchy_good $i $PS 0) && remove21
#		CRS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
#		[ "$CRS_ENC" != "" ] && SUM_ENC=$(($SUM_ENC+$CRS_ENC))
#		[ "$CRS_DEC" != "" ] && SUM_DEC=$(($SUM_DEC+$CRS_DEC))
#	done
#	[ $SUM_ENC -ne 0 ] && CRS_ENC=$(($SUM_ENC/$ITER))
#	[ $SUM_DEC -ne 0 ] && CRS_DEC=$(($SUM_DEC/$ITER))
#
#	################
#	SUM_ENC=0
#	SUM_DEC=0
#	[ $i -eq 32 ] &&
#	for ii in `seq 1 $ITER`;
#	do
#		CRS_ENC=$(./encoder $DEV $K $M cauchy_good $i $PS 0) && remove21
#		CRS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
#		[ "$CRS_ENC" != "" ] && SUM_ENC=$(($SUM_ENC+$CRS_ENC))
#		[ "$CRS_DEC" != "" ] && SUM_DEC=$(($SUM_DEC+$CRS_DEC))
#	done
#	[ $SUM_ENC -ne 0 ] && CRS_ENC=$(($SUM_ENC/$ITER))
#	[ $SUM_DEC -ne 0 ] && CRS_DEC=$(($SUM_DEC/$ITER))

#	#sleep 5
#	LINE_ENC="$i,$RS_ENC,$CRS_ENC"
#	LINE_DEC="$i,$RS_DEC,$CRS_DEC"
#	#[ -z $RS ] && WORD="$i $CRS"
#	echo $LINE_ENC >> $CSV_FILE_147_ENC
#	echo $LINE_DEC >> $CSV_FILE_147_DEC
#	echo "$LINE_ENC ::: $LINE_DEC"
#	unset LINE_ENC LINE_DEC RS_ENC RS_DEC CRS_ENC CRS_DEC
#done

cd $ED && rm -rf Coding && rm $DEV