#!/bin/bash

CSV_FILE=$PWD/../results/libfec/libfec_errors_dec.csv

CSD=$PWD/../libs/Libfec_Veritysetup/cryptsetup-master
LFD=$PWD/../libs/Libfec_Veritysetup/libfec-master/speed_tests
WD=$PWD

BS=4096
IMG=tst_dev
HSH=tst_hash
FEC=tst_fec

cd $CSD && dd if=/dev/urandom of=$IMG bs=1M count=500
rm $CSV_FILE

cd $CSD

for i in `seq 2 32`
do
	K=$((255-$i))

	# process of decoding in Veritysetup:
	#	- encode data
	#	- remeber the root hash for verification
	#	- corrupt maximum number of consecutive blocks (see corrupt_vs_dev.sh)
	#	- try to verify using root hash
	#	- verification fails -> try to decode using the encoded data
	#	- print Coding_speed: <coding_speed_in_mb_per_sec>
	#	- because the file size is 500MB and encoding/decoding speed is between 15 and 130 MB/sec, the variation is minimal
	if [ $(($i%2)) -eq 0 ] && [ $i -le 22 ] ;
	then
		cd $CSD

		ARR=(`./veritysetup --data-block-size $BS format $IMG $HSH --fec-device $FEC --fec-roots $i`)
		ROOT_HASH=${ARR[28]}

		cd $WD
		./corrupt_vs_dev.sh 255 $K

		cd $CSD
		DEC_VS=$(./veritysetup --data-block-size $BS verify $IMG $HSH $ROOT_HASH --fec-device $FEC --fec-roots $i --debug | grep -oP "(?<=Coding_speed: )[^ ]+")
	fi

	# measure speed of decoding errors of codes using two different polynomials
	cd $LFD
	DEC_CHAR_11d=$(./dec_errors_char_11d 255 $K)
	DEC_CHAR_187=$(./dec_errors_char_187 255 $K)

	# print results to stdout and to file
	LINE="$i,$DEC_CHAR_11d,$DEC_CHAR_187,$DEC_VS"
	echo $LINE >> $CSV_FILE
	echo $LINE
	unset DEC_VS DEC_CHAR_11d DEC_CHAR_187
done

cd $CSD && rm $IMG $HSH $FEC